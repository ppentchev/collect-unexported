# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

pytest >= 7, < 8
tomli-w >= 1, < 2
