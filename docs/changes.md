<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Changelog

All notable changes to the collect-unexported project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.1] - 2023-04-24

### Additions

- Add the Ringlet homepage, the GitLab repository, and the changelog to
  the `pyproject.toml` project metadata.

### Other changes

- Copy the contents of the documentation index page to the README file.

## [0.1.0] - 2023-04-24

### Started

- First public release.

[Unreleased]: https://gitlab.com/ppentchev/collect-unexported/-/compare/release%2F0.1.1...main
[0.1.1]: https://gitlab.com/ppentchev/collect-unexported/-/compare/release%2F0.1.0...release%2F0.1.1
[0.1.0]: https://gitlab.com/ppentchev/collect-unexported/-/tags/release%2F0.1.0
